﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClimaticChamberControl_GUI
{
    class PIDcontroller
    {
        private DataStore _ds;

        public SerialInterfaceUSB Siusb
        {
            get;
            set;
        }
        public PIDcontroller(DataStore ds)//for object updating
        {
            _ds = ds;
            pwm = new System.Windows.Threading.DispatcherTimer();
            //switching number of relay: 100000
            pwm.Interval = new TimeSpan(0, 0, 1);//intervall in (h,min,s) -> 1min is for 1666,7h operation time
            // 1/10 from 100% on, Exp.: 12 seconds * 10 = 120s period of pwm = 2 minutes period -> 2 switches for relay in one Period
            pwm.Tick += new EventHandler(pwm_Tick);
        }

        private System.Windows.Threading.DispatcherTimer CCTimer = new System.Windows.Threading.DispatcherTimer();
        public System.Windows.Threading.DispatcherTimer pwm;        
        
        public double SOLLtemp;       //from CCC_MainWindow
        public double SOLLhumi;        //from CCC_MainWindow
        public double ISTtempChiller;   //from SerialInterfaceModbus
        public double SOLLtempChiller = 18.0;//from SerialInterfaceModbus
        public double ISTtemp = 0;          //from SerialInterfaceUSB
        public double ISTabshumi = 0;       //from SerialInterfaceUSB
        //public double ISTrelhumi;         //from SerialInterfaceUSB

        public string Status = "shutdown";//shutdown, fill, on, off
        string StatusOld;
        int i = 0;
        int x = 0;
        int tick = 0;
        bool target = false;


        //general equation
        //Kp = Proportionalbeiwert; Ki = Integrierbeiwert; Kd = Differenzierbeiwert 
        //w -> Istwert
        //x -> Sollwert
        //e -> Regelabweichung
        //y -> Stellgröße
        //Ta -> Abtastzeit

        //for temperature controll
        double et = 0;      //system deviation
        double etsum = 0;   //system deviation summe
        double etold = 0;   //old system deviation
        double wt;          //actual value
        double xt;          //set value
        double yt;          //actuating value
        double Tat = 0.25;     //sampling time in s
        double Kpt = 17.86;         //P coefficient
        double Kit = 0.372;         //I coefficient
        double Kdt = 1.534;         //D coefficient
        double Kita;
        double Kdta;
        //for humidity controll
        double eh = 0;      //system deviation
        double ehsum = 0;   //system deviation summe
        double ehold = 0;   //old system deviation
        double wh;          //actual value
        double xh;          //set value
        double yh;          //actuating value
        double Tah = 0.25;     //sampling time in s
        double Kph = 17.86;         //P coefficient
        double Kih = 0.372;         //I coefficient
        double Kdh = 1.534;         //D coefficient
        double Kiha;
        double Kdha;
        double offset = 1.7;      //offset for generate same under and overshoot from set value

        private void TemperatureControl()
        {            
            wt = ISTtemp;
            xt = SOLLtemp;
            Kita = Kit * Tat;
            Kdta = Kdt / Tat;

            //Temperature PID controller
            et = wt - xt;
            etsum = etsum + et;
            yt = (Kpt* et) + (Kita * etsum) + (Kdta * (et - etold));
            etold = et;

        }

        private void HumidityControl()
        {            
            wh = ISTabshumi;
            xh = SOLLhumi;
            Kiha = Kih * Tah;
            Kdha = Kdh / Tah;

            //Humidity(absolute) PID controller
            eh = wh - xh;
            //ehsum = ehsum + eh;
            //yh = (Kph * eh) + (Kiha * ehsum) + (Kdha * (eh - ehold));
            //ehold = eh;
            yh = eh + offset;
        }

        public void ClimaticControl()
        {
            Kita = Kit * Tat;
            Kdta = Kdt / Tat;
            Kiha = Kih * Tah;
            Kdha = Kdh / Tah;

            if (i == 0)//first Session Start -> fill up the cylinder
            {
                Status = "fill";
                Siusb.Send(Status);
                //wait for "_ready" from MCU
                while (Siusb.fillStatus != true)
                {
                    System.Threading.Thread.Sleep(10);
                }
                Status = "off";
                Siusb.Send(Status);
                i = 1;
                x = 1;
            }
            Siusb.fillStatus = false;
            CCTimer.Interval = new TimeSpan(0,0,1);//intervall in (h,min,s)
            CCTimer.Tick += new EventHandler(CCTimer_Tick);
            CCTimer.Start();        
        }

        void CCTimer_Tick(object sender, EventArgs e)
        {
            //for writing  in textfile PWM on or off
            if (pwm.IsEnabled == true)
            {
                _ds.PWMcontrol = true;
            }
            if (pwm.IsEnabled == false)
            {
                _ds.PWMcontrol = false;
            }
            if (_ds.InOperation == true)
            {
                if (i == 20 * 60)//fill up the cylinder all TimeSpan * 30*60 = 30 min
                {
                    Status = "fill";
                    Siusb.Send(Status);
                    //wait for "_ready" from MCU
                    while (Siusb.fillStatus == false)
                    {
                        System.Threading.Thread.Sleep(10);
                    }
                    Siusb.Send(StatusOld);
                    i = 1;
                }
                if (x == 15)
                {
                    TemperatureControl();
                    SOLLtempChiller = SOLLtemp + (-yt);
                    if (SOLLtempChiller > 30)//maxTemp Chiller
                    {
                        SOLLtempChiller = 30;
                    }
                    else if (SOLLtempChiller < 5)//minTemp Chiller
                    {
                        SOLLtempChiller = 5;
                    }
                    x = 1;
                }

                //double humiOffset = -2;//in g/m^3, because high downtime
                double threshold = 1.6;//is IST 2g/m^3 under SOLL
                HumidityControl();
                //if (ISTtemp < 5)
                //{

                //}
                if (yh >= 0)
                {
                        target = true;
                    if (threshold <= ISTabshumi - SOLLhumi + offset)
                    {
                        pwm.IsEnabled = false;
                        tick = 0;
                        Status = "off";
                        Siusb.Send(Status);
                    }
                    else
                    {
                        pwm.IsEnabled = true;
                        
                    }
                    StatusOld = Status;
                }
                //if (eh > threshold)//value for stop PWM, because the new SOLL < old SOLL
                //{
                //    pwm.IsEnabled = false;
                //    target = false;
                //}
                if (yh < 0)//yh negativ -> need more steam
                {
                    if (target == false)
                    {
                        Status = "on";
                        StatusOld = Status;
                        Siusb.Send(Status);
                    }
                    else
                    {
                        if (threshold < ISTabshumi - SOLLhumi - offset)
                        {
                            pwm.IsEnabled = false;
                            tick = 0;
                            target = false;
                        }
                    }
                }
                i++;
                x++;
            }
        }

        void pwm_Tick(object sender, EventArgs e)
        {
            tick++;
            int SetPWM = 24;// on-share in %
            if (tick < Math.Round(Convert.ToDouble((SetPWM) / 0.8333), 2, MidpointRounding.AwayFromZero)) //PWM setting accuracy is 1%
            {
                //if (target == true)
                //{
                    Status = "on";
                    Siusb.Send(Status);
                //}
            }
            else
            {
                //target = true;// IST >= SOLL
                Status = "off";
                Siusb.Send(Status);
            }
            if (tick >= 120)
            {
                tick = 0;
            }
        }        
    }
}
