/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Fill_Level_Pin GPIO_PIN_0
#define Fill_Level_GPIO_Port GPIOA
#define Inflow_Pin GPIO_PIN_3
#define Inflow_GPIO_Port GPIOA
#define Steam_Cylinder_Pin GPIO_PIN_4
#define Steam_Cylinder_GPIO_Port GPIOA
#define LED_Steam_Pin GPIO_PIN_0
#define LED_Steam_GPIO_Port GPIOB
#define LED_Valve_Pin GPIO_PIN_1
#define LED_Valve_GPIO_Port GPIOB
#define LED_Control_Pin GPIO_PIN_8
#define LED_Control_GPIO_Port GPIOA
#define LED_Status_Pin GPIO_PIN_3
#define LED_Status_GPIO_Port GPIOB
#define LED_Error_Pin GPIO_PIN_4
#define LED_Error_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
void rx_callback(uint8_t *Buf, uint32_t *Len);
void Si7021_cli_init(uint8_t func);
void dewpointShutdown();
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
