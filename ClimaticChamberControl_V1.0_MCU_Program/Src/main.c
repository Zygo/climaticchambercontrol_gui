/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "rtc.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
//#include <string.h>
#include "stm32l0xx_hal_conf.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <math.h>
//#include <newlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern float temp,tempp;
extern float hu,humid;
extern int TEM;
extern int TEMkom;
extern int HUM;
extern int HUMkom;
uint8_t Fill = 0;
extern int data_ready;
extern char text[64];
int Status = 3;
int oldStatus = 3;
int Watchdog = 0;
int WatchdogControl = 0;
int oldWatchdogControl = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void clear_buf(uint8_t *pbyBuf, uint32_t dwLen);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_RTC_Init();
  MX_USB_DEVICE_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  HAL_Delay(1000);
  //HAL_I2C_Master_Transmit(&hi2c1,0x80,da,1,100);
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_Delay(500);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		//LED_Steam_Pin|LED_Valve_Pin|LED_Status_Pin|LED_Error_Pin;

	    if(Watchdog == 1)
	    {
	    	if (Status == 4)// Init
	    	{
	    		HAL_GPIO_WritePin(LED_Status_GPIO_Port, LED_Status_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_RESET);
				oldStatus = 4;
	    	}
			if (Status == 3)//DewpointShutDown
			{
				dewpointShutdown();
				oldStatus = 3;
			}
			if (Status == 2)//Zylinder f�llen
			{
				HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_RESET);

				while (HAL_GPIO_ReadPin(Fill_Level_GPIO_Port, Fill_Level_Pin) == 0)//Kontrolle: Dampfzylinder gef�llt?
				{
					HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_SET);
					HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_SET);

					//Blinkende Status LED f�r "Zylinder wird gef�llt"
					HAL_Delay(200);
					HAL_GPIO_TogglePin(LED_Status_GPIO_Port, LED_Status_Pin);
				}
				HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_RESET);

				Fill++;
				Status = oldStatus;
			}

			if (Status == 1)//Dampf an
			{
				HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED_Status_GPIO_Port, LED_Status_Pin, GPIO_PIN_SET);
				oldStatus = 1;
			}

			if (Status == 0)//Dampf aus
			{
				HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED_Status_GPIO_Port, LED_Status_Pin, GPIO_PIN_SET);
				oldStatus = 0;
			}
		}
	    else//shutdown
	    {
	    	HAL_GPIO_WritePin(LED_Status_GPIO_Port, LED_Status_Pin, GPIO_PIN_RESET);
	    	HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_RESET);
	    	HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_RESET);
	    	HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_RESET);
	    	HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_RESET);
	    	HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_SET);
	    }

		if (Fill == 1)
		{
		  CDC_Transmit_FS((uint8_t *)"DA_ready", 8);
		  Fill = 0;
		}
	    if (data_ready == 1)
	    {
			sprintf(text, "DAT%d,%d F%d,%d", TEM, TEMkom, HUM, HUMkom);
			CDC_Transmit_FS((uint8_t *)text, strlen(text));

	    	data_ready = 0;
	    }
	     /* USER CODE BEGIN 3 */

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }

  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_USB;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

//Ansteuerung der Regelung Dampferzeugung ja oder nein
void rx_callback(uint8_t *Buf, uint32_t *Len)
{
volatile uint8_t x = 0;
	while (x == 0)
	{
		if(!strcmp((char *)Buf, "initDA"))//Init Zustand
		{
			Status = 4;
		}
		if(!strcmp((char *)Buf, "shutdownDA"))//Shutdown Zustand
		{
			Status = 3;
		}
		if(!strcmp((char *)Buf, "fillDA"))//Zylinder f�llen
		{
			Status = 2;
		}
		if(!strcmp((char *)Buf, "onDA"))//an
		{
			Status = 1;
		}
		if(!strcmp((char *)Buf, "offDA"))//aus
		{
			Status = 0;
		}
		if(!strcmp((char *)Buf, "watchdogDA"))
		{
			Watchdog = 1;
			oldWatchdogControl = WatchdogControl - 2;
			WatchdogControl ++;
		}
		clear_buf(Buf, *Len);
		x = 1;
	}
}

void clear_buf(uint8_t *pbyBuf, uint32_t dwLen)
{
	uint8_t i;

	for(i = 0; i < dwLen; i++)
	{
		if(pbyBuf[i] != 0)
		{
			pbyBuf[i] = 0;
		}
		else
		{
			return;
		}
	}
}

void dewpointShutdown()
{
	HAL_GPIO_WritePin(LED_Status_GPIO_Port, LED_Status_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(Inflow_GPIO_Port, Inflow_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED_Valve_GPIO_Port, LED_Valve_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(Steam_Cylinder_GPIO_Port, Steam_Cylinder_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED_Steam_GPIO_Port, LED_Steam_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED_Error_GPIO_Port, LED_Error_Pin, GPIO_PIN_SET);
	Status = 3;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
