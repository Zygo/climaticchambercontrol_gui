﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClimaticChamberControl_GUI
{
    class SerialInterfaceUSB
    {
        public bool connect = false;//if USB connected
        public bool fillstatus = false;//if fillig activ or inactiv
        string[] dataTH;
        string[] da;
        public string temp;
        public string rhumi;
        public string absHumi;
        public string comPortNameUSB;

        CCC_MainWindow GUILink
        {
            get;
            set;
        }
        DataStore DATALink
        {
            get;
            set;
        }
        public PIDcontroller PIDLink
        {
            get;
            set;
        }

        public SerialInterfaceUSB(CCC_MainWindow _guiLink, DataStore _ds)//for object updating
        {
            GUILink = _guiLink;
            DATALink = _ds;
        }

        SerialPort ComPortUSB = new SerialPort(); // ComCort generating

        public static readonly Object signalLock = new Object();//for save sending (only one Signal is sending)

        public void actDA()//updating GUI Label
        {
            while (connect == true)
            {
                begin:

                string dataraw = "";
                while (dataraw == "")
                {
                    try
                    {
                        try
                        {
                            ComPortUSB.NewLine = "DA";//signal for new line/line beginning
                            dataraw = ComPortUSB.ReadLine();//raw data from MCU
                        }
                        catch (InvalidOperationException)
                        {
                            //connect = false;
                            //System.Windows.MessageBox.Show("Mikrocontrollerverbindung wurde unterbrochen!\nRegelung wurde abgeschaltet.\nAnwendung muss neu gestartet werden.");
                            //break;
                        }
                        catch (Exception)
                        { }
                    }
                    catch (IOException)
                    {
                        //for Close ComPort, because ReadLine is blocking 
                    }

                }
                if (connect == false)
                {
                    break;
                }
                if (dataraw == "_ready")// for differentiate the fillig status
                {
                    fillstatus = true;
                    goto begin;
                }
                dataTH = dataraw.Split(' ');

                for (int i = 0; i < 2; i++)// for differentiate and give the actual values for temperature and humidity
                {
                    if (dataTH[i].Substring(0, 1) == "T")
                    {
                        da = dataTH[i].Split('T');
                        temp = da[1];
                        temp = temp.Substring(0, 4);
                        GUILink.Temperature = temp;
                        if (DATALink != null)
                        {
                            DATALink.Temperature = temp.Replace(',', '.');
                            PIDLink.ISTtemp = Convert.ToDouble(temp);
                        }
                    }
                    if (dataTH[i].Substring(0, 1) == "F")
                    {
                        da = dataTH[i].Split('F');
                        rhumi = da[1];
                        rhumi = rhumi.Substring(0, 4);
                        if (Convert.ToDouble(rhumi) >= 100)
                        {
                            rhumi = "100,0";
                        }
                        GUILink.RelativeHumidity = rhumi;
                        if (DATALink != null)
                        {
                            DATALink.relHumidity = rhumi.Replace(',', '.');
                            //PIDLink.ISTrelhumi = Convert.ToDouble(rhumi.Replace(',', '.'));
                        }
                    }
                }

                //calculate the absolute humidity

                //TK = temperature in kelvin
                //TD = dew point in °C
                //DD = vopour pressure in hPa
                //SDD = saturation vapour pressure in hPa

                double mw = 18.016;//universal gas constant
                double rd = 8314.3;//molecular weight of water vapour
                double a = 7.5;//for steam T>=0
                double b = 237.3;//for steam T>=0

                double SDD = 6.1078 * Math.Pow(10, ((a * Convert.ToDouble(temp)) / (b + Convert.ToDouble(temp))));
                double DD = Convert.ToDouble(rhumi) / 100 * SDD;
                double v = Math.Log10(DD / 6.1078);
                double TD = b * v / (a - v);
                double AF = Math.Pow(10, 5) * mw / rd * DD / (Convert.ToDouble(temp) + 273.15);
                double roundOff_AF = Math.Round(AF, 1, MidpointRounding.AwayFromZero);
                GUILink.AbsoluteHumidity = roundOff_AF.ToString(absHumi);
                PIDLink.ISTabshumi = Convert.ToDouble(roundOff_AF.ToString(absHumi));
                PIDLink.ISTrelhumi = Convert.ToDouble(rhumi);
                if (DATALink != null)
                {
                    DATALink.absHumidity = roundOff_AF.ToString(absHumi).Replace(',', '.');
                    DATALink.DewPoint = Math.Round(TD, 1, MidpointRounding.AwayFromZero).ToString().Replace(',', '.');
                }

                //if realtive humidity over 95 % -> shut down if the automic activ
                if (Convert.ToDouble(rhumi) > 95)
                {
                    if (GUILink.p.DewPointShutDown == 1)
                    {
                        try
                        {
                            DATALink.InOperation = false;
                            DATALink.writeTimer.Stop();//stop writing in parameter text file
                            System.Windows.MessageBox.Show("System nahe des Taupunktes!\nRegelung wurde abgeschaltet.");
                            //GUILink.SollTempValue.IsEnabled = true;
                            //GUILink.SollabsHumiValue.IsEnabled = true;
                            DATALink.writeTimerDiagram.Stop();//stop writing in parameter text file
                            //GUILink.Connect.IsEnabled = false;
                            //GUILink.StartValue.IsEnabled = true;
                            //GUILink.StopValue.IsEnabled = false;
                            //GUILink.StopCurve.IsEnabled = false;
                            //GUILink.StartCurve.IsEnabled = true;
                            //GUILink.LoadCurve.IsEnabled = true;
                            //GUILink.SaveConfig.IsEnabled = true;
                            //GUILink.ValueTab.IsEnabled = true;
                            //GUILink.CurveTab.IsEnabled = true;
                            //GUILink.ConfigTab.IsEnabled = true;
                            PIDLink.SOLLtempChiller = 18.0;
                            Thread.Sleep(1000);
                            GUILink.ChillerTimer.Stop();
                            if (GUILink.updatingSollValue.IsEnabled == true)
                            {
                                GUILink.updatingSollValue.Stop();
                            }
                            lock (signalLock)
                            {
                                PIDLink.status = "shutdown";
                                Send(PIDLink.status);
                            }
                            connect = false;
                        }
                        catch (InvalidOperationException)
                        { }
                        break;
                    }
                }
            }
        }

        public void Disconnect()
        {
            try
            {
                PIDLink.SOLLtempChiller = 18.0;
                Thread.Sleep(200);
                lock (signalLock)
                {
                    ComPortUSB.WriteLine("shutdown");
                    Thread.Sleep(200);
                }
            }
            catch (InvalidOperationException)
            { }
            GUILink.WatchDog.Stop();
            ComPortUSB.Close();
        }

        public void Connect()
        {
            //ComPort properties
            ComPortUSB.PortName = comPortNameUSB;
            ComPortUSB.BaudRate = 115200;
            ComPortUSB.Parity = Parity.None;
            ComPortUSB.DataBits = 8;
            ComPortUSB.StopBits = StopBits.One;

            try
            {
                ComPortUSB.ReadTimeout = 1000;     //Wartezeit (Timeout Zeit) (in ms) auf Antwort bevor der Lesevorgang abgebrochen wird
                ComPortUSB.Open();
            }
            catch (Exception ex)
            {
                GUILink.WatchDog.Stop();
                ComPortUSB.Close();
                throw ex;
            }
        }

        public void Send(string status)//to send the value of status
        {
            try
            {
                lock (signalLock)
                {
                    ComPortUSB.WriteLine(status);//an jeden Befehl wird als Ende 'DA' angefügt
                }
            }
            catch (Exception ex)
            {
                GUILink.SollTempValue.IsEnabled = true;
                GUILink.SollabsHumiValue.IsEnabled = true;
                DATALink.writeTimer.Stop();//stop writing in parameter text file
                GUILink.Connect.IsEnabled = true;
                GUILink.StartValue.IsEnabled = false;
                GUILink.StopValue.IsEnabled = false;
                GUILink.StartCurve.IsEnabled = false;
                GUILink.StopCurve.IsEnabled = false;
                string msg = ex.Message;
                System.Windows.MessageBox.Show(string.Format("USB-Connection unterbrochen!"), "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                DATALink.InOperation = false;
                connect = false;
                GUILink.WatchDog.Stop();
                Disconnect();
            }
        }
        public void WatchDogSignal()
        {
            try
            {
                lock (signalLock)
                {
                    ComPortUSB.WriteLine("watchdog");//an jeden Befehl wird als Ende 'DA' angefügt
                }
            }
            catch (Exception ex)
            {
                GUILink.SollTempValue.IsEnabled = true;
                GUILink.SollabsHumiValue.IsEnabled = true;
                DATALink.writeTimer.Stop();//stop writing in parameter text file
                GUILink.Connect.IsEnabled = true;
                GUILink.StartValue.IsEnabled = false;
                GUILink.StopValue.IsEnabled = false;
                GUILink.StartCurve.IsEnabled = false;
                GUILink.StopCurve.IsEnabled = false;
                string msg = ex.Message;
                System.Windows.MessageBox.Show(string.Format("USB-Connection unterbrochen!"), "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                DATALink.InOperation = false;
                connect = false;
                GUILink.WatchDog.Stop();
                Disconnect();
            }


        }
    }
}
