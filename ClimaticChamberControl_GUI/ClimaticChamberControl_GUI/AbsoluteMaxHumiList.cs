﻿using ClimaticChamberControl_GUI.JSONconfig;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimaticChamberControl_GUI
{
    public class AbsoluteMaxHumiList
    {
        public IDictionary WriteDataList(ConfigParam p)//calculate and wirte a table with the dew point for min temperature to max temperature
        {
            double mw = 18.016;//universal gas constant
            double rd = 8314.3;//molecular weight of water vapour
            double a = 7.5;//for steam T>=0
            double b = 237.3;//for steam T>=0

            Dictionary<double, double> maxAbsHum = new Dictionary<double, double>();

            for (double i = Convert.ToDouble(p.MinTemp); i <= Convert.ToDouble(p.MaxTemp); i += 0.1)
            {
                i = Math.Round(i, 1, MidpointRounding.AwayFromZero);
                double AF = Math.Pow(10, 5) * mw / rd * (Convert.ToDouble(100) / 100 * (6.1078 * Math.Pow(10, ((a * Convert.ToDouble(i)) / (b + Convert.ToDouble(i)))))) / (Convert.ToDouble(i) + 273.15);
                AF = Math.Round(AF, 1, MidpointRounding.AwayFromZero);

                maxAbsHum.Add(i, AF);
            }
            return maxAbsHum;
        }
    }
}
