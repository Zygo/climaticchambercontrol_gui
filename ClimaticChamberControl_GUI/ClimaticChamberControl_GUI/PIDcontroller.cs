﻿using ClimaticChamberControl_GUI.JSONconfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimaticChamberControl_GUI
{
    class PIDcontroller
    {
        private DataStore _ds;
        private ConfigParam _p;

        public SerialInterfaceUSB Siusb
        {
            get;
            set;
        }
        public PIDcontroller(DataStore ds, ConfigParam p)//for object updating
        {
            _p = p;
            _ds = ds;
            pwm = new System.Windows.Threading.DispatcherTimer();
            //switching number of relay: 100000
            pwm.Interval = new TimeSpan(0, 0, 1);//intervall in (h,min,s) -> 1min is for 1666,7h operation time
            // 1/10 from 100% on, Exp.: 2 seconds * 120 = 240s period of pwm = 4 minutes period -> 2 switches for relay in one Period
            pwm.Tick += new EventHandler(pwm_Tick);
        }

        public System.Windows.Threading.DispatcherTimer CCTimer = new System.Windows.Threading.DispatcherTimer();
        public System.Windows.Threading.DispatcherTimer pwm;
        public System.Windows.Threading.DispatcherTimer heatingflow = new System.Windows.Threading.DispatcherTimer();

        public static readonly Object signalLock = new Object();//for save sending (only one Signal is sending)

        public double SOLLtemp;       //from CCC_MainWindow
        public double SOLLhumi;        //from CCC_MainWindow
        public double ISTtempChiller;   //from SerialInterfaceModbus
        public double SOLLtempChiller = 18.0;//from SerialInterfaceModbus
        public double ISTtemp = 0;          //from SerialInterfaceUSB
        public double ISTabshumi = 0;       //from SerialInterfaceUSB
        public double ISTrelhumi = 0;       //from SerialInterfaceUSB

        public string status;//init, shutdown, fill, on, off
        string statusOld;
        int i = 0;
        int x = 0;
        int tick = 0;
        bool target = false;//if true, the SOLL is near; if false, the SOLL is far
        bool firstStart = true;//by first start is the water cold -> 16 Min heating flow
        double firstHumi;//first humidity value for heating flow shutdown
        int h;//timer value for heating flow
        double oldSollTemp = 1;
        double oldSollHumi = 1;

        //for temperature controll
        double et = 0;      //system deviation
        double etsum = 0;   //system deviation summe
        double etold = 0;   //old system deviation
        double wt;          //actual value
        double xt;          //set value
        double yt;          //actuating value
        double Tat = 0.25;     //sampling time in s
        public double Kpt;  //P coefficient
        public double Kit;  //I coefficient
        public double Kdt;  //D coefficient
        double Kita;
        double Kdta;
        //for humidity controll
        double eh = 0;      //system deviation
        double wh;          //actual value
        double xh;          //set value
        double yh;          //actuating value
        public double offset;       //offset for generate same under and overshoot from set value
        public double threshold;    //threshold must be >= offset
        public int SetPWM;          //means ON Time in %
        public int NumberRelaisSwitch;

        private void TemperatureControl()
        {
            wt = ISTtemp;
            xt = SOLLtemp;
            Kita = Kit * Tat;
            Kdta = Kdt / Tat;

            if (oldSollTemp != xt)//set control to begin when new setpoint
            {
                etsum = 0;
                oldSollTemp = xt;
            }
            //Temperature PID controller
            et = wt - xt;
            etsum = etsum + et;
            yt = (Kpt * et) + (Kita * etsum) + (Kdta * (et - etold));
            etold = et;
            oldSollTemp = xt;

        }

        private void HumidityControl()
        {
            wh = ISTabshumi;
            xh = SOLLhumi;

            //Humidity(absolute) controller with zwo-point-control and PWM
            eh = wh - xh;
            yh = eh + offset;
            oldSollHumi = xh;
        }

        public void ClimaticControl()
        {
            if (i == 0)//first Session Start -> fill up the cylinder
            {
                lock (signalLock)
                {
                    status = "fill";
                    Siusb.Send(status);

                    //wait for "_ready" from MCU
                    while (Siusb.fillstatus != true)
                    {
                        System.Threading.Thread.Sleep(10);
                    }
                    status = "off";
                    Siusb.Send(status);
                    statusOld = status;
                }
                i = 1;
                x = 1;
            }
            Siusb.fillstatus = false;
            CCTimer.Interval = new TimeSpan(0, 0, 1);//intervall in (h,min,s)
            CCTimer.Tick += new EventHandler(CCTimer_Tick);
            heatingflow.Interval = new TimeSpan(0, 0, 1);
            heatingflow.Tick += new EventHandler(heatingflow_Tick);
            CCTimer.Start();//start the controller timer
        }

        void CCTimer_Tick(object sender, EventArgs e)
        {
            if (_ds.InOperation == true)
            {
                if (i == 30 * 60)//fill up the cylinder all TimeSpan * 15*60 = 15 min
                {
                    lock (signalLock)
                    {
                        status = "fill";
                        Siusb.Send(status);
                        //wait for "_ready" from MCU
                        while (Siusb.fillstatus == false)
                        {
                            System.Threading.Thread.Sleep(10);
                        }
                        Siusb.Send(statusOld);
                        if (statusOld == "on")
                        {
                            NumberRelaisSwitch += 2;
                        }
                        statusOld = status;
                    }
                    i = 1;
                }
                if (x == 15)//-> 1/4 minute -> Ta for Temp-PID = 0,25
                {
                    TemperatureControl();
                    SOLLtempChiller = SOLLtemp + (-yt);
                    if (SOLLtempChiller > 30)//maxTemp Chiller
                    {
                        SOLLtempChiller = 30;
                    }
                    else if (SOLLtempChiller < 5)//minTemp Chiller
                    {
                        SOLLtempChiller = 5;
                    }
                    x = 1;
                }

                HumidityControl();

                if (firstStart == true)//for heating flow for cylinder water, if the first SOLL 
                {
                    firstHumi = ISTabshumi;
                    firstStart = false;
                    heatingflow.Start();
                }
                if (heatingflow.IsEnabled == false)//if heating flow end -> start the controll
                {
                    if (SOLLhumi >= 5)
                    {
                        if (ISTrelhumi < 80.0)//safety loop because temperature is slower as humidity
                        {
                            //humidity control
                            if (yh >= -4 + offset)//PWM works when system 3g/m^3 under the SOLL
                            {
                                target = true;// IST >= SOLL            
                                if (threshold <= ISTabshumi - SOLLhumi + offset)
                                {
                                    lock (signalLock)
                                    {
                                        pwm.IsEnabled = false;
                                        tick = 0;
                                        status = "off";
                                        if (statusOld != status)
                                        {
                                            NumberRelaisSwitch++;
                                        }
                                        Siusb.Send(status);
                                        statusOld = status;
                                    }
                                }
                                else
                                {
                                    pwm.IsEnabled = true;

                                }
                            }
                            if (oldSollHumi != xh)//set control to begin when new setpoint //value for stop PWM, because the new SOLL < old SOLL
                            {
                                oldSollHumi = xh;
                                pwm.IsEnabled = false;
                                target = false;
                            }
                            if (yh < -4 + offset)//SOLL is to far -> full steam production
                            {
                                if (target == false)
                                {
                                    lock (signalLock)
                                    {
                                        status = "on";
                                        if (statusOld != status)
                                        {
                                            NumberRelaisSwitch++;
                                        }
                                        Siusb.Send(status);
                                        statusOld = status;
                                    }
                                }
                                else
                                {
                                //    if (threshold < ISTabshumi - SOLLhumi - offset)
                                //    {
                                        pwm.IsEnabled = false;
                                        tick = 0;
                                        target = false;
                                    //}
                                }
                            }
                            i++;
                            x++;
                        }
                        else// if rel humidity over 80% -> shut down steam
                        {
                            lock (signalLock)
                            {
                                pwm.IsEnabled = false;
                                tick = 0;
                                status = "off";
                                if (statusOld != status)
                                {
                                    NumberRelaisSwitch++;
                                }
                                Siusb.Send(status);
                                statusOld = status;
                            }
                        }
                    }
                }
            }
        }
        void pwm_Tick(object sender, EventArgs e)
        {
            tick++;
            //SetPWM = on-share in %
            double denominator = 0.8333333333333;// if 100/highest tick -> here 120 -> 100/120=0.8333333
            double PWM = (SetPWM) / (denominator);
            if (tick < Math.Round(PWM, 2, MidpointRounding.AwayFromZero))
            {
                lock (signalLock)
                {
                    status = "on";
                    if (statusOld != status)
                    {
                        NumberRelaisSwitch++;
                    }
                    Siusb.Send(status);
                    statusOld = status;
                }
            }
            else
            {
                lock (signalLock)
                {
                    status = "off";
                    if (statusOld != status)
                    {
                        NumberRelaisSwitch++;
                    }
                    Siusb.Send(status);
                    statusOld = status;
                }
            }
            if (tick >= 120)
            {
                tick = 0;
            }
        }

        void heatingflow_Tick(object sender, EventArgs e)
        {
            lock (signalLock)
            {
                h++;
                status = "on";
                Siusb.Send(status);

                if (h >= 16 || firstHumi > ISTabshumi + 0.5)//the system need 17 minutes for heating up
                {
                    status = "off";
                    Siusb.Send(status);
                    statusOld = status;
                    NumberRelaisSwitch += 2;

                    heatingflow.Stop();
                }
            }
        }
    }
}
