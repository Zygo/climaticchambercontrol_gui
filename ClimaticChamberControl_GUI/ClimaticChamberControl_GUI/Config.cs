﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimaticChamberControl_GUI.JSONconfig
{
    public class ConfigParam//all parameters in the Config file
    {
        public int NumberRelaisSwitch { get; set; }
        public string OperatingLifeSteamcylinder { get; set; }
        public string OperatingLifeElectrode { get; set; }
        public int WriteDataIntervalMin { get; set; }
        public int WriteDataIntervalSek { get; set; }
        public int DiagramTimeMax { get; set; }
        public int COMPortNameUSB { set; get; }
        public int COMPortNameModbus { get; set; }
        public int WriteDataSollTemp { get; set; }
        public int WriteDataTemp { get; set; }
        public int WriteDataSollHum { get; set; }
        public int WriteDataRelHum { get; set; }
        public int WriteDataAbsHum { get; set; }
        public int WriteDataDewPoint { get; set; }
        public int DewPointShutDown { get; set; }
        public double MaxTemp { get; set; }
        public double MinTemp { get; set; }
        public double TemperaturP { get; set; }
        public double TemperaturI { get; set; }
        public double TemperaturD { get; set; }
        public double HumidityThreshold { get; set; }
        public double HumidityOffset { get; set; }
        public int HumidityPWMon { get; set; }
        public int UseOnlyMeasurementData { get; set; }

public static ConfigParam Load()
        {
            using (StreamReader sr = new StreamReader(@"CCC_Config.json"))
            {
                string jsonData = sr.ReadToEnd();
                return (ConfigParam)JsonConvert.DeserializeObject<ConfigParam>(jsonData);
            }
        }

        public void Save()
        {
            string objjsonData = JsonConvert.SerializeObject(this);
            File.WriteAllText(@"CCC_Config.json", objjsonData, Encoding.UTF8);
        }
    }
}
