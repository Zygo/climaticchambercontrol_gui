﻿using ClimaticChamberControl_GUI.JSONconfig;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using NLog;
using ModbusInterfaceLib;
using WinForms = System.Windows.Forms;// for FileExplorer_Click
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.Windows.Threading;
using Microsoft.Win32;

namespace ClimaticChamberControl_GUI
{
    public partial class CCC_MainWindow : Window
    {
        public ConfigParam p = new ConfigParam();
        AbsoluteMaxHumiList DataMaxHumi = new AbsoluteMaxHumiList();

        SerialInterfaceUSB _siusb;
        ModbusInterface_Base _mbInterface;
        DataStore _ds;
        PIDcontroller _pid;

        bool startThreadactDA = false;
        bool startThreadClimaticControl = false;
        bool ChillerConnected = false;
        bool newStart = false;
        //bool FirstTimerEvent = true;

        string comPortNameModbus;
        public DispatcherTimer ChillerTimer = new DispatcherTimer();
        public DispatcherTimer SaveRelaisSwitches = new DispatcherTimer();
        public DispatcherTimer WatchDog = new DispatcherTimer();

        Random jitterer = new Random();//for watchdog signal sending

        //public string[] interval;
        public int Interval_Min;
        public int Interval_Sek;
        //public string WriteInterval;


        ObservableCollection<DataPoint> Data { get; set; } = new ObservableCollection<DataPoint>();

        public PlotModel LinePlot
        { get; private set; }

        public new PlotController ToolTip
        { get; private set; }

        public LineSeries tempSoll = new LineSeries();
        public LineSeries humiSoll = new LineSeries();
        public LineSeries tempIst = new LineSeries();
        public LineSeries humiIst = new LineSeries();

        int oldTimeValue = -1;
        private int _selectedPoint = -1;
        int diagramInterval = 3;
        double maxAbsHumidity;
        public bool start = true;

        //private ViewModels.MainWindowModel viewModel;

        public CCC_MainWindow()
        {
            _ds = new DataStore(this, _pid, p);
            _pid = new PIDcontroller(_ds, p);
            _siusb = new SerialInterfaceUSB(this, _ds);
            _siusb.PIDLink = _pid;
            _pid.Siusb = _siusb;
            _ds.InOperation = false;

            LinePlot = new PlotModel { };
            p = ConfigParam.Load();

            IDictionary dmHL = DataMaxHumi.WriteDataList(p);

            _siusb.comPortNameUSB = "COM" + Convert.ToString(p.COMPortNameUSB);
            comPortNameModbus = "COM" + Convert.ToString(p.COMPortNameModbus);

            ChillerTimer.Interval = new TimeSpan(0, 0, 15); //intervall in (h,min,s)
            SaveRelaisSwitches.Interval = new TimeSpan(0, 0, 1);
            WatchDog.Interval = new TimeSpan(0, 0, 1);
            ChillerTimer.Tick += new EventHandler(ChillerTimer_Tick);
            SaveRelaisSwitches.Tick += new EventHandler(SaveRelaisSwitches_Tick);
            WatchDog.Tick += new EventHandler(WatchDog_Tick);

            //////////////////////////////////////////////////////////////////////OxyPlot Diagramm
            var TemperatureAxis = new LinearAxis
            {
                Position = AxisPosition.Right,
                Title = "Temperatur [°C]",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                Maximum = p.MaxTemp + 0.1,
                Minimum = p.MinTemp,
                TitleColor = OxyColors.Red,
                IsZoomEnabled = false,
                Key = "tempkey"
            };
            var HumidityAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                Title = "Absolute Luftfeuchte [g/m³]",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dash,
                Maximum = Convert.ToDouble(dmHL[p.MaxTemp]),//from DataList MaxAbsHum
                Minimum = 0,
                TitleColor = OxyColors.Blue,
                IsZoomEnabled = false,
                Key = "humikey"
            };

            var TimeAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Title = "Zeit [h]",
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                AbsoluteMaximum = p.DiagramTimeMax,
                Minimum = 0,
                TitleColor = OxyColors.Black,
                IsZoomEnabled = false,
                Key = "timekey"
            };

            LinePlot.Axes.Add(TemperatureAxis);
            LinePlot.Axes.Add(HumidityAxis);
            LinePlot.Axes.Add(TimeAxis);

            tempSoll.YAxisKey = TemperatureAxis.Key;
            humiSoll.YAxisKey = HumidityAxis.Key;
            tempIst.YAxisKey = TemperatureAxis.Key;
            humiIst.YAxisKey = HumidityAxis.Key;

            tempSoll.Title = "Temperatur";
            humiSoll.Title = "Luftfeuchte";
            tempSoll.Color = OxyColors.Red;
            humiSoll.Color = OxyColors.Blue;
            tempSoll.MarkerType = MarkerType.Circle;
            humiSoll.MarkerType = MarkerType.Circle;
            tempSoll.MarkerStrokeThickness = 2;
            humiSoll.MarkerStrokeThickness = 2;
            tempSoll.MarkerStroke = OxyColors.Red;
            humiSoll.MarkerStroke = OxyColors.Blue;

            tempIst.Title = "Reale Temperatur";
            humiIst.Title = "Reale Luftfeuchte";
            tempIst.Color = OxyColors.LightPink;
            humiIst.Color = OxyColors.LightSkyBlue;

            //Mouse Control
            double xPos;
            double yPos;
            double xinterval;

            double diagramRightX = 558.93;
            double diagramLeftX = 56.93;
            double diagramLowerY = 355.04;
            double diagramUpperY = 10.04;
            double diagramWidthOnScreen = diagramRightX - diagramLeftX;
            int numberOfElementsX;

            double maxAbsHumidity_rel;

            //////////////Move Mouse In Diagramm
            //selecting the point
            LinePlot.MouseDown += (s, e) =>
            {
                if (_ds.InOperation == false)
                {
                    numberOfElementsX = tempSoll.Points.Count;

                    // only handle the left mouse button (right button can still be used to pan)
                    if (e.ChangedButton == OxyMouseButton.Left)
                    {
                        xPos = e.Position.X;
                        yPos = e.Position.Y;

                        xinterval = diagramWidthOnScreen / (numberOfElementsX - 1); // width of middle elements (left and right are each with half width)

                        if ((xPos <= diagramRightX && xPos >= diagramLeftX) && (yPos <= diagramLowerY && yPos >= diagramUpperY))
                        {
                            double mouseRelativeToDiagramX = xPos - diagramLeftX;
                            _selectedPoint = (int)((mouseRelativeToDiagramX / xinterval) + 0.5); // add half to fix first and last element having half width              
                        }
                        e.Handled = true;
                    }
                }
            };
            //change the new position from selected point
            LinePlot.MouseMove += (s, e) =>
            {
                if (_ds.InOperation == false)
                {
                    if (_selectedPoint != -1)
                    {
                        if (SelectedTemperature.IsChecked == true)
                        {
                            double tmpx = tempSoll.Points[_selectedPoint].X;
                            double tmpy = tempSoll.Points[_selectedPoint].Y;
                            double newY = TemperatureAxis.Minimum + (e.Position.Y - diagramLowerY) * (TemperatureAxis.Maximum - TemperatureAxis.Minimum) / (diagramUpperY - diagramLowerY);
                            if (newY >= p.MaxTemp)
                            {
                                newY = p.MaxTemp;
                            }
                            if (newY <= p.MinTemp)
                            {
                                newY = p.MinTemp;
                            }
                            tempSoll.Points.RemoveAt(_selectedPoint);
                            tempSoll.Points.Insert(_selectedPoint, new DataPoint(tmpx, newY));
                            LinePlot.InvalidatePlot(true);

                        }
                        if (SelectedHumidity.IsChecked == true)
                        {
                            //new point position
                            double tmpx = humiSoll.Points[_selectedPoint].X;
                            double tmpy = humiSoll.Points[_selectedPoint].Y;
                            double newY = HumidityAxis.Minimum + (e.Position.Y - diagramLowerY) * (HumidityAxis.Maximum - HumidityAxis.Minimum) / (diagramUpperY - diagramLowerY);
                            double actTemp = Math.Round(tempSoll.Points[_selectedPoint].Y, 1, MidpointRounding.AwayFromZero);
                            maxAbsHumidity_rel = Convert.ToDouble(dmHL[actTemp]);
                            if (DewPointShutDown.IsEnabled == true)//20% less than dew point
                            {
                                maxAbsHumidity_rel = maxAbsHumidity_rel / 100 * 80;
                                maxAbsHumidity_rel = Math.Round(maxAbsHumidity_rel, 1, MidpointRounding.AwayFromZero);
                            }
                            if (newY >= maxAbsHumidity_rel)
                            {
                                newY = maxAbsHumidity_rel;
                            }
                            if (newY <= 0)
                            {
                                newY = 0;
                            }
                            humiSoll.Points.RemoveAt(_selectedPoint);
                            humiSoll.Points.Insert(_selectedPoint, new DataPoint(tmpx, newY));
                            LinePlot.InvalidatePlot(true);

                            //show relative humidity
                            double x = -(2.30259 * a * actTemp) / (b + actTemp);
                            double RF = 1 / mw * 2.68522 * Math.Pow(10, -17) * rd * (6097274619555 * actTemp + 1665470562331448) * newY * Math.Pow(euler, x);
                            double roundOff_RF = Math.Round(RF, 1, MidpointRounding.AwayFromZero);
                            relHumi_from_point.Content = "relative Feuchte (Stunde " + _selectedPoint * 3 + "): " + roundOff_RF + " %";
                        }
                    }

                    e.Handled = true;
                }
            };

            LinePlot.MouseUp += (s, e) =>
            {
                if (_ds.InOperation == false)
                {
                    _selectedPoint = -1;
                    e.Handled = true;
                }
            };

            InitializeComponent();
            SelectedTemperature.IsChecked = true;

            LinePlot.Series.Add(tempSoll);
            LinePlot.Series.Add(humiSoll);
            LinePlot.Series.Add(tempIst);
            LinePlot.Series.Add(humiIst);

            ///////////////////////////////////////////////////////////////////////////////Config Load to Program Start
            if (p.WriteDataSollTemp == 1)
            {
                WriteSollTemp.IsChecked = true;
            }
            else
            {
                WriteSollTemp.IsChecked = false;
            }
            if (p.WriteDataTemp == 1)
            {
                WriteTemp.IsChecked = true;
            }
            else
            {
                WriteTemp.IsChecked = false;
            }
            if (p.WriteDataSollHum == 1)
            {
                WriteSollHum.IsChecked = true;
            }
            else
            {
                WriteSollHum.IsChecked = false;
            }
            if (p.WriteDataAbsHum == 1)
            {
                WriteAbsHum.IsChecked = true;
            }
            else
            {
                WriteAbsHum.IsChecked = false;
            }
            if (p.WriteDataRelHum == 1)
            {
                WriteRelHum.IsChecked = true;
            }
            else
            {
                WriteRelHum.IsChecked = false;
            }
            if (p.WriteDataDewPoint == 1)
            {
                WriteDewPoint.IsChecked = true;
            }
            else
            {
                WriteDewPoint.IsChecked = false;
            }
            if (p.DewPointShutDown == 1)
            {
                DewPointShutDown.IsChecked = true;
            }
            else
            {
                DewPointShutDown.IsChecked = false;
            }
            if (p.UseOnlyMeasurementData == 1)
            {
                UseOnlyMeasurementData.IsChecked = true;
            }
            else
            {
                UseOnlyMeasurementData.IsChecked = false;
            }
            timeSoll.Maximum = Convert.ToInt32(p.DiagramTimeMax);
            maxtimeValue.Content = "(max.: " + Convert.ToString(p.DiagramTimeMax) + ")";
            MaxTemp.Value = p.MaxTemp;
            MinTemp.Value = p.MinTemp;
            SollTempValue.Maximum = p.MaxTemp;
            SollTempValue.Minimum = p.MinTemp;
            MaxTime.Value = Convert.ToInt16(p.DiagramTimeMax);
            Interval_Min = p.WriteDataIntervalMin;
            Interval_Sek = p.WriteDataIntervalSek;

            IntervalMin.Value = Interval_Min;
            IntervalSek.Value = Interval_Sek;

            COMUSB.Value = Convert.ToInt16(p.COMPortNameUSB);
            COMModbus.Value = Convert.ToInt16(p.COMPortNameModbus);

            maxAbsHumidity = Convert.ToDouble(dmHL[p.MaxTemp]);
            if (DewPointShutDown.IsEnabled == true)//20% less than dew point
            {
                maxAbsHumidity = maxAbsHumidity / 100 * 80;
                maxAbsHumidity = Math.Round(maxAbsHumidity, 1, MidpointRounding.AwayFromZero);
            }
            else
            {
                maxAbsHumidity = Math.Round(maxAbsHumidity, 1, MidpointRounding.AwayFromZero);
            }
            SollabsHumiValue.Maximum = Convert.ToInt32(maxAbsHumidity);

            ///////////////////////////////////////////////////////////////////////////////Load PID and all other Parameters
            _pid.Kpt = p.TemperaturP;
            _pid.Kit = p.TemperaturI;
            _pid.Kdt = p.TemperaturD;
            _pid.SetPWM = p.HumidityPWMon;
            _pid.threshold = p.HumidityThreshold;
            _pid.offset = p.HumidityOffset;
            _pid.NumberRelaisSwitch = p.NumberRelaisSwitch;
            LifetimeCylinder.Content = "letzter Dampfzylinderaustausch: " + p.OperatingLifeSteamcylinder;
            LifetimeElectrode.Content = "letzter Elektrodenwechsel: " + p.OperatingLifeElectrode;
            
            start = false;
            newStart = false;
            SaveConfig.IsEnabled = false;
            ///////////////////////////////////////////////////////////////////////////////give Warning for excange the relais
            //Datasheet: el. lifetime = 100000 switching cycles and mech. lifetime = 10000000 switching cycles
            string warningInfo = "Das Relais muss ausgetauscht werden!\n\n Wurde das Relais ausgetauscht?";
            string errorInfo = "Das Koppelrelais hat " + Convert.ToString(p.NumberRelaisSwitch) + " Schaltspiele überlebt!\n Das Relais muss ausgetauscht werden, bevor die Regelung betrieben werden kann!";

            if (p.NumberRelaisSwitch >= 95000)// only Warning
            {
                var reponse = MessageBox.Show(string.Format(warningInfo), "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (reponse == MessageBoxResult.Yes)
                {
                    p.NumberRelaisSwitch = 0;
                    _pid.NumberRelaisSwitch = 0;
                    p.Save();
                }
            }
            if(p.NumberRelaisSwitch >= 10000)// error, stop the program
            {
                MessageBox.Show(string.Format(errorInfo), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Exit += CurrentOnExit;
            }
            //set GUI Buttons and Tabs off, if UseOnlyMeasurementData activ
            if (UseOnlyMeasurementData.IsChecked == true)
            {
                SollTempValue.IsEnabled = false;
                SollabsHumiValue.IsEnabled = false;
                SollrelHumiValue.IsEnabled = false;

                CurveTab.IsEnabled = false;
            }
        }

        private void user_Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _siusb.connect = false;
            _ds.InOperation = false;
            
            Application.Current.Exit += CurrentOnExit;
        }
        private void CurrentOnExit(object sender, ExitEventArgs exitEventArgs)
        {
            //final things before the app shuts down
            _siusb.Disconnect();
            p.Save();
        }

        ///////////////////////////////////////////////////////////////////////////////Value Tab
        private void SollTemp_Value(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (SollTempValue.Value >= MaxTemp.Value)
            {
                SollTempValue.Value = MaxTemp.Value;
            }
            if (SollTempValue.Value <= MinTemp.Value)
            {
                SollTempValue.Value = MinTemp.Value;
            }
        }

        double mw = 18.016;//universal gas constant
        double rd = 8314.3;//molecular weight of water vapour
        double a = 7.5;//for steam T>=0
        double b = 237.3;//for steam T>=0
        double euler = 2.718281828459045235;

        private void SollabsHumi_Value(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (start == false)
            {
                IDictionary dmHL = DataMaxHumi.WriteDataList(p);
                
                //Check min and max
                if (MaxTemp != null)
                {
                    double maxAbsHumidity_rel = Convert.ToDouble(dmHL[Convert.ToDouble(SollTempValue.Value)]);
                    if (DewPointShutDown.IsEnabled == true)//20% less than dew point
                    {
                        maxAbsHumidity_rel = maxAbsHumidity_rel / 100 * 80;
                        maxAbsHumidity_rel = Math.Round(maxAbsHumidity_rel, 1, MidpointRounding.AwayFromZero);
                    }
                    if (SollabsHumiValue.Value <= 0)
                    {
                        SollabsHumiValue.Value = 0;
                    }
                    if (SollabsHumiValue.Value >= maxAbsHumidity_rel)
                    {
                        SollabsHumiValue.Value = Convert.ToDouble(maxAbsHumidity_rel);
                    }
                }
                //calculate relHumi
                double x = -(2.30259 * a * Convert.ToDouble(SollTempValue.Value)) / (b + Convert.ToDouble(SollTempValue.Value));
                double RF = 1 / mw * 2.68522 * Math.Pow(10, -17) * rd * (6097274619555 * Convert.ToDouble(SollTempValue.Value) + 1665470562331448) * Convert.ToDouble(SollabsHumiValue.Value) * Math.Pow(euler, x);
                double roundOff_RF = Math.Round(RF, 1, MidpointRounding.AwayFromZero);
                SollrelHumiValue.Value = roundOff_RF;
            }
        }
        private void SollrelHumi_Value(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (start == false)
            {
                if (DewPointShutDown.IsEnabled == true)//5% less than dew point
                {
                    if (SollrelHumiValue.Value >= 80)
                    {
                        SollrelHumiValue.Value = 80.0;
                    }
                }
                //calculate absHumi
                double SDD = 6.1078 * Math.Pow(10, ((a * Convert.ToDouble(SollTempValue.Value)) / (b + Convert.ToDouble(SollTempValue.Value))));
                double DD = Convert.ToDouble(SollrelHumiValue.Value) / 100 * SDD;
                double v = Math.Log10(DD / 6.1078);
                double TD = b * v / (a - v);
                double AF = Math.Pow(10, 5) * mw / rd * DD / (Convert.ToDouble(SollTempValue.Value) + 273.15);
                double roundOff_AF = Math.Round(AF, 1, MidpointRounding.AwayFromZero);
                SollabsHumiValue.Value = roundOff_AF;
            }
        }

        private void Start_Value(object sender, RoutedEventArgs e)//start button
        {
            if (SollTempValue.Text.ToString() == "")
            {
                MessageBox.Show(string.Format("Es müssen noch SOLL-Valuee gesetzt werden."), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (SaveLocation.Text.ToString() == "")
            {
                MessageBox.Show(string.Format("Es muss noch ein Pfad gewählt werden."), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                _ds.Path = SaveLocation.Text;
                _ds.InOperation = true;
                StopValue.IsEnabled = true;
                StartValue.IsEnabled = false;
                SollTempValue.IsEnabled = false;
                SollabsHumiValue.IsEnabled = false;
                SollrelHumiValue.IsEnabled = false;
                StopCurve.IsEnabled = false;
                StartCurve.IsEnabled = false;
                LoadCurve.IsEnabled = false;
                SaveConfig.IsEnabled = false;
                ValueTab.IsEnabled = true;
                CurveTab.IsEnabled = false;
                ConfigTab.IsEnabled = false;
                fill.IsEnabled = false;
                _pid.status = "off";
                _siusb.Send(_pid.status);

                _ds.WriteInterval = new TimeSpan(0, p.WriteDataIntervalMin, p.WriteDataIntervalSek);
                _ds.GenerateFile();
                _ds.StoreDATA();
                //set functions only, if UseOnlyMeasurementData inactiv
                if (UseOnlyMeasurementData.IsChecked == false)
                {
                    _pid.SOLLtemp = Convert.ToDouble(SollTempValue.Text);
                    _pid.SOLLhumi = Convert.ToDouble(SollabsHumiValue.Text);

                    if (startThreadClimaticControl == false)
                    {
                        _pid.ClimaticControl();
                        startThreadClimaticControl = true;
                    }
                    SaveRelaisSwitches.Start();
                    //start timer for control settingWorkingTemperature
                    ChillerTimer.Start();
                }
            }
        }

        private void Stop_Value(object sender, RoutedEventArgs e)// stop button
        {
            _siusb.connect = true;
            _ds.InOperation = false;
            SollTempValue.IsEnabled = true;
            SollabsHumiValue.IsEnabled = true;
            SollrelHumiValue.IsEnabled = true;
            _ds.writeTimer.Stop();//stop writing in parameter text file
            Connect.IsEnabled = false;
            StartValue.IsEnabled = true;
            StopValue.IsEnabled = false;
            StopCurve.IsEnabled = false;
            StartCurve.IsEnabled = true;
            LoadCurve.IsEnabled = true;
            SaveConfig.IsEnabled = true;
            ValueTab.IsEnabled = true;
            //set functions only, if UseOnlyMeasurementData inactiv
            if (UseOnlyMeasurementData.IsChecked == false)
            {
                CurveTab.IsEnabled = true;
                fill.IsEnabled = true;
            }
            ConfigTab.IsEnabled = true;
            _pid.heatingflow.Stop();
            _pid.CCTimer.Stop();
            _pid.pwm.Stop();
            _pid.status = "off";
            _siusb.Send(_pid.status);
            Thread.Sleep(100);
            SaveRelaisSwitches.Stop();
            _pid.SOLLtempChiller = 18.0;
            Thread.Sleep(500);
            ChillerTimer.Stop();
            startThreadClimaticControl = false;
        }

        ///////////////////////////////////////////////////////////////////////////////Curve Tab
        //with Tooltip show the relative Humidity from Point
        public DispatcherTimer updatingSollValue = new DispatcherTimer();
        bool updatingSollValueEvent = true;
        DateTime startTime;
        DateTime nowTime;
        int RunHours = 0;
        int Point = 0;
        string[] value;
        double Value;

        private void Start_Curve(object sender, RoutedEventArgs e)// start button
        {
            if (SaveLocation.Text.ToString() == "")
            {
                MessageBox.Show(string.Format("Es muss noch ein Pfad gewählt werden."), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {                
                _ds.GenerateCurve(humiSoll, tempSoll);
                _ds.Path = SaveLocation.Text;
                _ds.InOperation = true;
                StopValue.IsEnabled = false;
                StartValue.IsEnabled = false;
                SollTempValue.IsEnabled = false;
                SollabsHumiValue.IsEnabled = false;
                SollrelHumiValue.IsEnabled = false;
                StopCurve.IsEnabled = true;
                StartCurve.IsEnabled = false;
                LoadCurve.IsEnabled = false;
                SaveConfig.IsEnabled = false;
                CurveTab.IsEnabled = true;
                ValueTab.IsEnabled = true;
                ConfigTab.IsEnabled = false;
                fill.IsEnabled = false;
                _pid.status = "off";
                _siusb.Send(_pid.status);

                _ds.WriteInterval = new TimeSpan(0, p.WriteDataIntervalMin, p.WriteDataIntervalSek);
                _ds.GenerateFile();
                _ds.StoreDATA();
                _ds.StoreDATADiagram();
                startTime = DateTime.Now;
                startTime = startTime.AddHours(3);

                tempIst.Points.Clear();
                humiIst.Points.Clear();
                //Set Setpoint
                Point = 0;
                value = Convert.ToString(tempSoll.Points.ElementAt(Point)).Split(new Char[] { ' ' });
                Value = Convert.ToDouble(value[1]);
                Value = Math.Round(Value, 1, MidpointRounding.AwayFromZero);
                _pid.SOLLtemp = Convert.ToDouble(Value);
                SollTempValue.Text = Convert.ToString(Value);//show actual SOLL in DoubleUpDown in first Tab
                value = Convert.ToString(humiSoll.Points.ElementAt(Point)).Split(new Char[] { ' ' });
                Value = Convert.ToDouble(value[1]);
                Value = Math.Round(Value, 1, MidpointRounding.AwayFromZero);
                _pid.SOLLhumi = Convert.ToDouble(Value);
                SollabsHumiValue.Text = Convert.ToString(Value);//show actual SOLL in DoubleUpDown in first Tab
                //Write IST point in Diagram
                tempIst.Points.Add(new DataPoint(0, Convert.ToDouble(_ds.Temperature.Replace('.', ','))));
                humiIst.Points.Add(new DataPoint(0, Convert.ToDouble(_ds.absHumidity.Replace('.', ','))));
                LinePlot.InvalidatePlot(true);

                //show relative humidity
                double x = -(2.30259 * a * _pid.SOLLtemp) / (b + _pid.SOLLtemp);
                double RF = 1 / mw * 2.68522 * Math.Pow(10, -17) * rd * (6097274619555 * _pid.SOLLtemp + 1665470562331448) * _pid.SOLLhumi * Math.Pow(euler, x);
                double roundOff_RF = Math.Round(RF, 1, MidpointRounding.AwayFromZero);
                SollrelHumiValue.Text = Convert.ToString(roundOff_RF);

                updatingSollValue.Interval = new TimeSpan(0, 0, 10); //intervall in (h,min,s)
                if (updatingSollValueEvent == true)
                {
                    updatingSollValue.Tick += new EventHandler(updatingSollValue_Tick);
                    updatingSollValueEvent = false;
                }
                updatingSollValue.Start();

                if (startThreadClimaticControl == false)
                {
                    _pid.ClimaticControl();
                    startThreadClimaticControl = true;
                }
                SaveRelaisSwitches.Start();
                //start timer for control settingWorkingTemperature
                ChillerTimer.Start();

                relHumi_from_point.Content = "";
            }
        }
        void updatingSollValue_Tick(object sender, EventArgs e)// for delivered the actual SOLL to controller
        {
            nowTime = DateTime.Now;
            
            if (Point >= tempSoll.Points.Count - 1)// if end of Curve
            {
                _siusb.connect = true;
                _ds.InOperation = false;
                SollTempValue.IsEnabled = true;
                SollabsHumiValue.IsEnabled = true;
                SollrelHumiValue.IsEnabled = true;
                _ds.writeTimerDiagram.Stop();//stop writing in parameter text file
                Connect.IsEnabled = false;
                StartValue.IsEnabled = true;
                StopValue.IsEnabled = false;
                StopCurve.IsEnabled = false;
                StartCurve.IsEnabled = true;
                LoadCurve.IsEnabled = true;
                SaveConfig.IsEnabled = true;
                ValueTab.IsEnabled = true;
                CurveTab.IsEnabled = true;
                ConfigTab.IsEnabled = true;
                fill.IsEnabled = true;
                updatingSollValue.Stop();
                _pid.heatingflow.Stop();
                _pid.CCTimer.Stop();
                _pid.pwm.Stop();
                _pid.status = "off";
                _siusb.Send(_pid.status);
                Thread.Sleep(100);
                SaveRelaisSwitches.Stop();
                _pid.SOLLtempChiller = 18.0;
                Thread.Sleep(500);
                ChillerTimer.Stop();
                startThreadClimaticControl = false;

            }
            if (DateTime.Compare(nowTime, startTime) >= 0)
            {
                try
                {
                    RunHours += diagramInterval;
                    startTime = startTime.AddHours(3);
                    Point = RunHours / 3;
                    value = Convert.ToString(tempSoll.Points.ElementAt(Point)).Split(new Char[] { ' ' });
                    Value = Convert.ToDouble(value[1]);
                    Value = Math.Round(Value, 1, MidpointRounding.AwayFromZero);
                    _pid.SOLLtemp = Convert.ToDouble(Value);
                    SollTempValue.Text = Convert.ToString(Value);//show actual SOLL in DoubleUpDown in first Tab
                    value = Convert.ToString(humiSoll.Points.ElementAt(Point)).Split(new Char[] { ' ' });
                    Value = Convert.ToDouble(value[1]);
                    Value = Math.Round(Value, 1, MidpointRounding.AwayFromZero);
                    _pid.SOLLhumi = Convert.ToDouble(Value);
                    SollabsHumiValue.Text = Convert.ToString(Value);//show actual SOLL in DoubleUpDown in first Tab
                    //show relative humidity
                    double x = -(2.30259 * a * _pid.SOLLtemp) / (b + _pid.SOLLtemp);
                    double RF = 1 / mw * 2.68522 * Math.Pow(10, -17) * rd * (6097274619555 * _pid.SOLLtemp + 1665470562331448) * _pid.SOLLhumi * Math.Pow(euler, x);
                    double roundOff_RF = Math.Round(RF, 1, MidpointRounding.AwayFromZero);
                    SollrelHumiValue.Text = Convert.ToString(roundOff_RF);
                }
                catch (ArgumentOutOfRangeException)
                {
                    _siusb.connect = true;
                    _ds.InOperation = false;
                    SollTempValue.IsEnabled = true;
                    SollabsHumiValue.IsEnabled = true;
                    SollrelHumiValue.IsEnabled = true;
                    _ds.writeTimerDiagram.Stop();//stop writing in parameter text file
                    Connect.IsEnabled = false;
                    StartValue.IsEnabled = true;
                    StopValue.IsEnabled = false;
                    StopCurve.IsEnabled = false;
                    StartCurve.IsEnabled = true;
                    LoadCurve.IsEnabled = true;
                    SaveConfig.IsEnabled = true;
                    ValueTab.IsEnabled = true;
                    CurveTab.IsEnabled = true;
                    ConfigTab.IsEnabled = true;
                    fill.IsEnabled = true;
                    updatingSollValue.Stop();
                    _pid.heatingflow.Stop();
                    _pid.CCTimer.Stop();
                    _pid.pwm.Stop();
                    _pid.status = "off";
                    _siusb.Send(_pid.status);
                    Thread.Sleep(100);
                    SaveRelaisSwitches.Stop();
                    _pid.SOLLtempChiller = 18.0;
                    Thread.Sleep(500);
                    ChillerTimer.Stop();
                    startThreadClimaticControl = false;
                }
            }
        }

        private void Stop_Curve(object sender, RoutedEventArgs e)// stop button
        {
            _siusb.connect = true;
            _ds.InOperation = false;
            SollTempValue.IsEnabled = true;
            SollabsHumiValue.IsEnabled = true;
            SollrelHumiValue.IsEnabled = true;
            _ds.writeTimerDiagram.Stop();//stop writing in parameter text file
            Connect.IsEnabled = false;
            StartValue.IsEnabled = true;
            StopValue.IsEnabled = false;
            StopCurve.IsEnabled = false;
            StartCurve.IsEnabled = true;
            LoadCurve.IsEnabled = true;
            SaveConfig.IsEnabled = true;
            ValueTab.IsEnabled = true;
            CurveTab.IsEnabled = true;
            ConfigTab.IsEnabled = true;
            fill.IsEnabled = true;         
            updatingSollValue.Stop();
            _pid.heatingflow.Stop();
            _pid.CCTimer.Stop();
            _pid.pwm.Stop();
            Thread.Sleep(100);
            _pid.status = "off";
            _siusb.Send(_pid.status);
            Thread.Sleep(100);
            SaveRelaisSwitches.Stop();
            _pid.SOLLtempChiller = 18.0;
            Thread.Sleep(500);
            ChillerTimer.Stop();
            startThreadClimaticControl = false;
        }
        bool LoadingCurve = false;
        private void Load_Curve(object sender, RoutedEventArgs e)// load button
        {
            LoadingCurve = true;
            string CurveFilePath = null;
            //find file to load
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "txt files (*.txt)|*.txt";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                CurveFilePath = openFileDialog.FileName;
            }
            //read file and add points
            //index 0: time (X)
            //index 1: temperature (Y)
            //index 2: humidity (Y)
            if (CurveFilePath != null)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(CurveFilePath))
                    {
                        //delete Points
                        tempSoll.Points.Clear();
                        humiSoll.Points.Clear();

                        string CurveFile = sr.ReadToEnd();
                        string[] CurveData = CurveFile.Split('\n');
                        string[] CurvePoints;
                        for (int line = 1; line < CurveData.Count() - 1; line++)
                        {
                            CurvePoints = CurveData[line].Split(',');
                            CurvePoints[1] = CurvePoints[1].Replace('.', ',');
                            CurvePoints[2] = CurvePoints[2].Replace('.', ',');
                            CurvePoints[2] = CurvePoints[2].Replace('\r', ' ');
                            tempSoll.Points.Add(new DataPoint(Convert.ToDouble(CurvePoints[0]), Convert.ToDouble(CurvePoints[1])));
                            humiSoll.Points.Add(new DataPoint(Convert.ToDouble(CurvePoints[0]), Convert.ToDouble(CurvePoints[2])));
                        }
                        timeSoll.Value = (CurveData.Count() - 3) * 3;
                    }
                    LinePlot.InvalidatePlot(true);
                    LoadingCurve = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Datei besitzt Formatierungsfehler und kann nicht eingelesen werden"), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    timeSoll.Value = 15;
                }
            }
        }

        private void SetXAxis(object sender, RoutedPropertyChangedEventArgs<object> e)// running time setting in diagram
        {
            if (LoadingCurve == false)
            {
                if (timeSoll.Value <= 3)
                {
                    timeSoll.Value = 3;
                }
                if (timeSoll.Value >= p.DiagramTimeMax)
                {
                    timeSoll.Value = p.DiagramTimeMax;
                }

                if ((timeSoll.Value) <= p.DiagramTimeMax)
                {
                    if (oldTimeValue > timeSoll.Value)
                    {
                        //remove some elements
                        tempSoll.Points.RemoveAll(p => p.X > Convert.ToInt32(timeSoll.Value));
                        humiSoll.Points.RemoveAll(p => p.X > Convert.ToInt32(timeSoll.Value));
                    
                        oldTimeValue = Convert.ToInt32(timeSoll.Value);
                    }
                    if (oldTimeValue < timeSoll.Value)
                    {

                        for (int i = (oldTimeValue) + 1; i <= timeSoll.Value; i++)
                        {
                            if ((i) % diagramInterval == 0)
                            {
                                tempSoll.Points.Add(new DataPoint(i, 22));
                                humiSoll.Points.Add(new DataPoint(i, 0));
                            }
                        }
                        oldTimeValue = Convert.ToInt32(timeSoll.Value);
                    }
                    LinePlot.InvalidatePlot(true);
                }
            }
            else
            {
                LoadingCurve = false;
            }
        }

        //for only on Box can be checked 
        private void UncheckT(object sender, RoutedEventArgs e)
        {
            SelectedHumidity.IsChecked = true;
        }
        private void CheckT(object sender, RoutedEventArgs e)
        {
            SelectedHumidity.IsChecked = false;
        }
        private void UncheckH(object sender, RoutedEventArgs e)
        {
            SelectedTemperature.IsChecked = true;
        }
        private void CheckH(object sender, RoutedEventArgs e)
        {
            SelectedTemperature.IsChecked = false;
        }

///////////////////////////////////////////////////////////////////////////////Config Tab
        private void Config_Speichern(object sender, RoutedEventArgs e)// save button
        {
            if (WriteSollTemp.IsChecked == true)
            {
                p.WriteDataSollTemp = 1;
            }
            else
            {
                p.WriteDataSollTemp = 0;
            }
            if (WriteTemp.IsChecked == true)
            {
                p.WriteDataTemp = 1;
            }
            else
            {
                p.WriteDataTemp = 0;
            }
            if (WriteSollHum.IsChecked == true)
            {
                p.WriteDataSollHum = 1;
            }
            else
            {
                p.WriteDataSollHum = 0;
            }
            if (WriteAbsHum.IsChecked == true)
            {
                p.WriteDataAbsHum = 1;
            }
            else
            {
                p.WriteDataAbsHum = 0;
            }
            if (WriteRelHum.IsChecked == true)
            {
                p.WriteDataRelHum = 1;
            }
            else
            {
                p.WriteDataRelHum = 0;
            }
            if (WriteDewPoint.IsChecked == true)
            {
                p.WriteDataDewPoint = 1;
            }
            else
            {
                p.WriteDataDewPoint = 0;
            }
            if (DewPointShutDown.IsChecked == true)
            {
                p.DewPointShutDown = 1;
            }
            else
            {
                p.DewPointShutDown = 0;
            }
            if (UseOnlyMeasurementData.IsChecked == true)
            {
                p.UseOnlyMeasurementData = 1;
            }
            else
            {
                p.UseOnlyMeasurementData = 0;
            }
            p.MaxTemp = Convert.ToDouble(MaxTemp.Value);
            p.MinTemp = Convert.ToDouble(MinTemp.Value);
            p.DiagramTimeMax = Convert.ToInt32(MaxTime.Value);
            Interval_Min = Convert.ToInt32(IntervalMin.Value);
            Interval_Sek = Convert.ToInt32(IntervalSek.Value);

            p.WriteDataIntervalMin = Interval_Min;
            p.WriteDataIntervalSek = Interval_Sek;

            p.COMPortNameUSB = Convert.ToInt16(COMUSB.Value);
            _siusb.comPortNameUSB = "COM" + Convert.ToString(COMUSB.Value);
            p.COMPortNameModbus = Convert.ToInt16(COMModbus.Value);
            comPortNameModbus = "COM" + Convert.ToString(COMModbus.Value);

            p.Save();
            if (newStart == true)
            {
                MessageBox.Show(string.Format("System muss neu gestartet werden,\n\num die Einstellungen zu übernehmen!"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            SaveConfig.IsEnabled = false;
            newStart = false;
        }
        private void Config_newValue(object sender, RoutedEventArgs e)
        {
            SaveConfig.IsEnabled = true;
        }
        private void Config_newValues(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SaveConfig.IsEnabled = true;
        }
        private void Config_newValue_newStart(object sender, RoutedEventArgs e)
        {
            SaveConfig.IsEnabled = true;
            newStart = true;
        }
        private void Config_newValues_newStart(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SaveConfig.IsEnabled = true;
            newStart = true;
        }
        ///////////////////////////////////////////////////////////////////////////////Maintenance Tab
        private void fill_Click(object sender, RoutedEventArgs e)// fill button
        {
            _siusb.Send("fill");
            while (_siusb.fillstatus == false)
            {
                Thread.Sleep(10);
            }
            _siusb.Send("off");
        }

        ///////////////////////////////////////////////////////////////////////////////Over the Tab
        private void Connect_Click(object sender, RoutedEventArgs e)//connect button
        {
            try
            {
                _siusb.connect = true;
                _siusb.Connect();
                if (startThreadactDA == false)
                {
                    Thread thrUSB = new Thread(new ThreadStart(_siusb.actDA));
                    thrUSB.Start();
                    startThreadactDA = true;
                }
                StartValue.IsEnabled = true;
                StopValue.IsEnabled = false;
                Connect.IsEnabled = false;
                //set functions only, if UseOnlyMeasurementData inactiv
                if (UseOnlyMeasurementData.IsChecked == false)
                {
                    InitChiller();
                    StartCurve.IsEnabled = true;
                    StopCurve.IsEnabled = false;
                    fill.IsEnabled = true;
                    Thread.Sleep(10);
                    _pid.status = "init";
                    _siusb.Send(_pid.status);
                }
                WatchDog.Start();
            }
            catch (Exception ex)
            {
                _siusb.connect = true;
                _siusb.Disconnect();
                Connect.IsEnabled = true;
                StartValue.IsEnabled = false;
                StartCurve.IsEnabled = false;
                StopValue.IsEnabled = false;
                StopCurve.IsEnabled = false;
                fill.IsEnabled = false;
                WatchDog.Stop();
                ChillerConnected = false;
                MessageBox.Show(string.Format("Error: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        void WatchDog_Tick(object sender, EventArgs e)// timer so say the MCU the program is running
        {
            _siusb.WatchDogSignal();
            WatchDog.Interval = TimeSpan.FromSeconds(1) + TimeSpan.FromMilliseconds(jitterer.Next(0, 10));
        }
        private void FileExplorer_Click(object sender, RoutedEventArgs e)// ... button
        {
            WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = true;
            folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            WinForms.DialogResult result = folderDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                _ds.Path = folderDialog.SelectedPath;
                SaveLocation.Text = _ds.Path;
            }
        }
        private static void OpenExplorer(string path)
        {
            if (Directory.Exists(path))
            {
                System.Diagnostics.Process.Start("explorer.exe", path);
            }
        }

        public string Temperature// for updating the real temperature in GUI
        {
            get
            {
                return temp.Content.ToString();
            }
            set
            {
                if (temp.Dispatcher.CheckAccess())
                {
                    this.temp.Content = value;
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.temp.Content = value));
                }
            }
        }
        public string RelativeHumidity// for updating the real relative humidity in GUI
        {
            get
            {
                return rhumi.Content.ToString();
            }
            set
            {
                if (rhumi.Dispatcher.CheckAccess())
                {
                    this.rhumi.Content = value;
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.rhumi.Content = value));
                }
            }
        }
        public string AbsoluteHumidity// for updating the real absolute humidity in GUI
        {
            get
            {
                return abshumi.Content.ToString();
            }
            set
            {
                if (abshumi.Dispatcher.CheckAccess())
                {
                    this.abshumi.Content = value;
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.abshumi.Content = value));
                }
            }
        }

        void ChillerTimer_Tick(object sender, EventArgs e)// give the chiller the actual temperature
        {
            try
            {
                if (_ds.InOperation == true)
                {

                    if (ChillerConnected == true)
                    {
                        var workTemp = GetWorkingTemperature();
                        Thread.Sleep(100);
                        _pid.ISTtempChiller = Convert.ToDouble(workTemp);
                        var bathTemp = GetBathTemperature();
                        Thread.Sleep(100);

                        var controller_status = GetControllerstatus();
                        Thread.Sleep(100);

                        double settingWorkingTemperature = _pid.SOLLtempChiller;//Set Temperature
                        SetWorkingTemperature(settingWorkingTemperature);
                        Thread.Sleep(100);

                        workTemp = GetWorkingTemperature();
                        Thread.Sleep(100);
                    }
                }
                else
                {
                    _siusb.connect = false;
                    _ds.InOperation = false;
                    SollTempValue.IsEnabled = true;
                    SollabsHumiValue.IsEnabled = true;
                    _ds.writeTimer.Stop();//stop writing in parameter text file
                    Connect.IsEnabled = true;
                    StartValue.IsEnabled = false;
                    StopValue.IsEnabled = false;
                    StartCurve.IsEnabled = false;
                    StopCurve.IsEnabled = false;
                    WatchDog.Stop();
                    _siusb.Disconnect();
                    MessageBox.Show(string.Format("Chillerverbindung unterbrochen.\nRegelung beendet."), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception)
            {
                //string msg = ex.Message;
            }
        }

        void SaveRelaisSwitches_Tick(object sender, EventArgs e)//write the number of switches from the cylinder relais
        {
            p.NumberRelaisSwitch = _pid.NumberRelaisSwitch;
            p.Save();
        }

        public void setTopline_ds()//write the line for data store
        {
            _ds.dataTopline = "";
            if (p.WriteDataTemp == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Celsius(°C)";
            }
            if (p.WriteDataRelHum == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Humidity(%rh)";
            }
            if (p.WriteDataDewPoint == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Dew Point(°C)";
            }
            if (p.WriteDataAbsHum == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Absolute Humidity(g/m^3)";
            }
            if (p.WriteDataSollTemp == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Setpoint Temperature(°C)";
            }
            if (p.WriteDataSollHum == 1)
            {
                _ds.dataTopline = _ds.dataTopline + ",Setpoint Humidity(g/m^3)";
            }
        }
        public void setDataline_ds()//write the line for data store
        {
            _ds.data = "";
            if (p.WriteDataTemp == 1)
            {
                _ds.data = _ds.data + "," + _ds.Temperature;
            }
            if (p.WriteDataRelHum == 1)
            {
                _ds.data = _ds.data + "," + _ds.relHumidity;
            }
            if (p.WriteDataDewPoint == 1)
            {
                _ds.data = _ds.data + "," + _ds.DewPoint;
            }
            if (p.WriteDataAbsHum == 1)
            {
                _ds.data = _ds.data + "," + _ds.absHumidity;
            }
            if (p.WriteDataSollTemp == 1)
            {
                _ds.data = _ds.data + "," + _pid.SOLLtemp;
            }
            if (p.WriteDataSollHum == 1)
            {
                _ds.data = _ds.data + "," + _pid.SOLLhumi;
            }
        }
        ////////////////////////////////////////////////////////////NatinalLabChiller
        private static Logger log = LogManager.GetCurrentClassLogger();

        byte _modbusAddress = 0;

        private void InitChiller()
        {
            string currentMethod = "Init";
            try
            {
                // close current interface if already connected
                try
                {
                    if (_mbInterface != null)
                    {
                        ModbusConnectionManager.Instance.CloseModbusInterface(_modbusAddress, _mbInterface);
                        _mbInterface = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("[{0}] - error closing existing interface. Proceeding anyway. ex: {1}", currentMethod, ex.Message);
                }

                _modbusAddress = 1;

                _mbInterface = ModbusConnectionManager.Instance.GetModbusInterface(_modbusAddress, comPortNameModbus, 9600, 8, System.IO.Ports.StopBits.One, System.IO.Ports.Parity.None, 150, 150);

                if (_mbInterface.IsConnected)
                {
                    var workTemp = GetWorkingTemperature();
                    _pid.ISTtempChiller = Convert.ToDouble(workTemp);
                    var bathTemp = GetBathTemperature();

                    var controller_status = GetControllerstatus();

                    //log.Info("[{0}] - NationalLab: bath temperature={1}°C  working temperature={2}°C   controller_status={3}", currentMethod, bathTemp, workTemp, controller_status);

                    //double settingWorkingTemperature = 18.0;//Set Temperature
                    //SetWorkingTemperature(settingWorkingTemperature);
                    ChillerConnected = true;
                }
                else
                {
                    ChillerConnected = false;
                    //throw new Exception("Couldn't connect to serial port.");
                    MessageBox.Show(string.Format("Couldn't connect to serial port."), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                ChillerConnected = false;
                log.Error("[{0}] - ex: {1}", currentMethod, ex.Message);
                throw;
            }
        }

        void CheckConnectionState()
        {
            if (_mbInterface == null || _mbInterface.IsConnected == false)
            {
                //throw new Exception("Connection to cooling device not available.");
                _siusb.connect = false;
                SollTempValue.IsEnabled = true;
                SollabsHumiValue.IsEnabled = true;
                _ds.writeTimer.Stop();//stop writing in parameter text file
                Connect.IsEnabled = true;
                StartValue.IsEnabled = false;
                StopValue.IsEnabled = false;
                StartCurve.IsEnabled = false;
                StopCurve.IsEnabled = false;
                WatchDog.Stop();
                _siusb.Disconnect();
                MessageBox.Show(string.Format("Chillerverbindung unterbrochen.\nRegelung beendet."), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        // NationalLab ProfiCool specific communication

        // 0 : off
        // 1 : auto control
        // 2 : tuning
        // 3 : man. control
        protected int GetControllerstatus()
        {
            CheckConnectionState();

            int status = -1;
            try
            {
                var ret = _mbInterface.ReadHoldingRegisters(_modbusAddress, 0x20F, 1);
                status = ret[0];
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("NationalLab: Error getting controller status. ex:{0}", ex.Message));
            }

            return status;
        }

        protected double GetBathTemperature()
        {
            CheckConnectionState();

            double retTemp = 0;
            try
            {
                var retData = _mbInterface.ReadHoldingRegisters(_modbusAddress, 0x200, 1);
                retTemp = retData[0] / 10.0;
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("NationalLab: Error getting current bath temperature. ex:{0}", ex.Message));
            }

            return retTemp;
        }

        protected double GetWorkingTemperature()
        {
            CheckConnectionState();

            double retTemp = 0;
            try
            {
                var retData = _mbInterface.ReadHoldingRegisters(_modbusAddress, 0x208, 1);
                retTemp = retData[0] / 10.0;
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("NationalLab: Error getting working temperature. ex:{0}", ex.Message));
            }

            return retTemp;
        }

        protected void SetWorkingTemperature(double workingTemperature)
        {
            CheckConnectionState();

            ushort val = Convert.ToUInt16(workingTemperature * 10.0);
            _mbInterface.WriteSingleRegister(_modbusAddress, 0x2802, val);
            RecalculateInternalChecksum();
        }

        protected void RecalculateInternalChecksum()
        {
            CheckConnectionState();
            // just write to address 0x039B to start the checksum calculation
            _mbInterface.WriteSingleRegister(_modbusAddress, 0x039B, 1);
        }

    }
}
