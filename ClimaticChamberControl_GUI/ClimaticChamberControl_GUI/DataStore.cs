﻿using ClimaticChamberControl_GUI.JSONconfig;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimaticChamberControl_GUI
{
    class DataStore
    {
        CCC_MainWindow GUILink
        {
            get;
            set;
        }
        public PIDcontroller PIDLink
        {
            get;
            set;
        }
        public ConfigParam p
        {
            get;
            private set;
        }

        public DataStore(CCC_MainWindow _guiLink, PIDcontroller _pidLink, ConfigParam _p)
        {
            GUILink = _guiLink;
            PIDLink = _pidLink;
            p = _p;
        }

        public string Path
        {
            get;
            set;
        }
        public bool InOperation
        {
            get;
            set;
        }
        public TimeSpan WriteInterval
        {
            get;
            set;
        }

        string Time;
        string Date;
        string filename;
        string filenameCurve;
        UInt32 i = 1;
        UInt32 i2 = 1;
        bool FirstTimerEvent = true;
        bool FirstTimerDiagramEvent = true;
        double DiagramTimeMin = 0;
        double DiagramTimeHour = 0;

        public string DewPoint;
        public string Temperature;
        public string relHumidity;
        public string absHumidity;

        public string data;
        public string dataTopline;

        public System.Windows.Threading.DispatcherTimer writeTimer = new System.Windows.Threading.DispatcherTimer();
        public System.Windows.Threading.DispatcherTimer writeTimerDiagram = new System.Windows.Threading.DispatcherTimer();

        public void GenerateFile()
        {
            i = 1;
            try
            {
                Date = DateTime.Now.ToString("yyyyMMdd");
                filename = Path + "/" + Date + "_CCC_Parameter.txt";
                GUILink.setTopline_ds();
                if (File.Exists(filename))
                {
                    if (System.Windows.Forms.MessageBox.Show("Datei ersetzen?", "Datei exsitiert bereits!", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        File.Delete(filename);
                        using (StreamWriter sw = File.CreateText(filename))
                        {
                            sw.WriteLine(Date + ",Time" + dataTopline);
                        }
                    }
                    else
                    {
                        InOperation = false;
                    }
                }
                else
                {
                    //generate file and write first line
                    using (StreamWriter sw = File.CreateText(filename))
                    {
                        sw.WriteLine(Date + ",Time" + dataTopline);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void StoreDATA()//write parameters in new Line 1 and start Writer
        {
            //WriteInterval = "00:00:60";
            writeTimer.Interval = WriteInterval - TimeSpan.FromMilliseconds(10); //intervall in (h,min,s)
            if (FirstTimerEvent == true)
            {
                writeTimer.Tick += new EventHandler(writeTimer_Tick);
                FirstTimerEvent = false;
            }
            if (InOperation == true)
            {
                Time = DateTime.Now.ToString("HH:mm:ss");
                Date = DateTime.Now.ToString("yyyy-MM-dd");

                GUILink.setDataline_ds();
                using (StreamWriter sw = File.AppendText(filename))
                {
                    sw.WriteLine("1," + Date + " " + Time + data);
                }

                writeTimer.Start();
            }
        }
        void writeTimer_Tick(object sender, EventArgs e)//write a new line with actual data
        {
            FileInfo fi = new FileInfo(filename);

            i++;
            Time = DateTime.Now.ToString("HH:mm:ss");
            Date = DateTime.Now.ToString("yyyy-MM-dd");

            GUILink.setDataline_ds();
            using (StreamWriter sw = File.AppendText(filename))
            {
                sw.WriteLine(i.ToString() + "," + Date + " " + Time + data);
            }
        }
        
        public void StoreDATADiagram()// write a new data point in the curve diagram
        {
            //start Timer
            writeTimerDiagram.Interval = new TimeSpan(0, 0, 60) - TimeSpan.FromMilliseconds(10); //intervall in (h,min,s)
            if (FirstTimerDiagramEvent == true)
            {
                writeTimerDiagram.Tick += new EventHandler(writeTimerDiagram_Tick);
                FirstTimerDiagramEvent = false;
            }
            writeTimerDiagram.Start();
        }
        void writeTimerDiagram_Tick(object sender, EventArgs e)
        {
            DiagramTimeMin++;
            GUILink.tempIst.Points.Add(new DataPoint(DiagramTimeHour + DiagramTimeMin / 60, Convert.ToDouble(Temperature.Replace('.', ','))));
            GUILink.humiIst.Points.Add(new DataPoint(DiagramTimeHour + DiagramTimeMin / 60, Convert.ToDouble(absHumidity.Replace('.', ','))));            
            if (DiagramTimeMin >= 60)
            {
                DiagramTimeHour++;
                DiagramTimeMin = 0;
            }
            GUILink.LinePlot.InvalidatePlot(true);
        }

        public void GenerateCurve(LineSeries humiSoll, LineSeries tempSoll)//Write the curve data in a file
        {
            int ListLength;
            string[] value;
            string temp;
            string humi;
            string time;
            ListLength = tempSoll.Points.Count;
            i2 = 1;
            try
            {
                Date = DateTime.Now.ToString("yyyyMMdd");
                filenameCurve = Path + "\\" + Date + "_CurveData.txt";
                if (File.Exists(filenameCurve))
                {
                    if (System.Windows.Forms.MessageBox.Show("Datei der Soll-Curvendaten ersetzen?", "Datei exsitiert bereits!", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        File.Delete(filenameCurve);
                        using (StreamWriter sw = File.CreateText(filenameCurve))
                        {
                            sw.WriteLine("Time(h),Celsius(°C),Absolute Humidity(g/m^3)");
                            for (int point = 0; point < ListLength; point++)
                            {
                                value = Convert.ToString(tempSoll.Points.ElementAt(point)).Split(new Char[] { ' ' });
                                temp = value[1];
                                temp = Convert.ToString(Math.Round(Convert.ToDouble(temp), 1, MidpointRounding.AwayFromZero));
                                temp = temp.Replace(',', '.');
                                time = value[0];
                                time = time.Replace(',', '.');
                                value = Convert.ToString(humiSoll.Points.ElementAt(point)).Split(new Char[] { ' ' });
                                humi = value[1];
                                humi = Convert.ToString(Math.Round(Convert.ToDouble(humi), 1, MidpointRounding.AwayFromZero));
                                humi = humi.Replace(',', '.');

                                sw.WriteLine(time + "," + temp + "," + humi);
                            }
                        }
                    }
                    else
                    {
                        InOperation = false;
                    }
                }
                else
                {
                    //generate file and write first line
                    using (StreamWriter sw = File.CreateText(filenameCurve))
                    {
                        sw.WriteLine("Time(h),Celsius(°C),Absolute Humidity(g/m^3);");
                        for (int point = 0; point < ListLength; point++)
                        {
                            value = Convert.ToString(tempSoll.Points.ElementAt(point)).Split(new Char[] { ' ' });
                            temp = value[1];
                            temp = Convert.ToString(Math.Round(Convert.ToDouble(temp), 1, MidpointRounding.AwayFromZero));
                            temp = temp.Replace(',', '.');
                            time = value[0];
                            time = time.Replace(',', '.');
                            value = Convert.ToString(humiSoll.Points.ElementAt(point)).Split(new Char[] { ' ' });
                            humi = value[1];
                            humi = Convert.ToString(Math.Round(Convert.ToDouble(humi), 1, MidpointRounding.AwayFromZero));
                            humi = humi.Replace(',', '.');

                            sw.WriteLine(time + "," + temp + "," + humi);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
    }
}
